<div align="center">

<p align="center">
  <a href="" rel="noopener">
    <img width="30%" src="./ansible-wordmark.png" alt="Ansible Wordmark"></a>
    <br>
    <img width="70%"src="./k3s-logo.png" alt="K3S logo">
  </a>
</p>

  [![Status](https://img.shields.io/badge/status-Active-success.svg)]() 
  [![Ansible Galaxy](https://img.shields.io/ansible/role/TODO)]() 
  [![Ansible Version](https://img.shields.io/badge/ansible-2.10-informational)]() 
  [![Ansible Version](https://img.shields.io/ansible/quality/TODO)]() 
  [![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](/LICENSE)

</div>

---

# System K3S Cluster Member

Sets up the requirements for a machine to become a member of a K3S cluster.

- Enables IPV4 and IPV6 Forwarding
- Enables cgroups (Raspberry Pi Only)
- Reverts to iptabbles-legacy (Raspberry Pi Only)
- Installs K3S binaries

## 🦺 Requirements

*None*

## 🗃️ Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

```yaml
    # Choose the version to be installed
    k3s_version: v1.19.4+k3s1
    ansible_user: ansible
```

## 📦 Dependencies

*None*

## 🤹 Example Playbook

*TODO*

## ⚖️ License

This code is released under the [GPLv3](./LICENSE) license. For more information refer to `LICENSE.md`

## ✍️ Author Information

- Heavely inspired from [K3S Ansible Role](https://github.com/k3s-io/k3s-ansible)

## References

- [i built a Raspberry Pi SUPER COMPUTER!! // ft. Kubernetes (k3s cluster w/ Rancher)](https://www.youtube.com/watch?v=X9fSMGkjtug)
- [k3sup](https://github.com/alexellis/k3sup)
